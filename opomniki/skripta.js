window.addEventListener('load', function() { //Zelo Pomembno! - Čakamo, da se spletna stran naloži - lahko bi zahtevali določeno vrednost, če se stran sploh ne bi naložila
	//stran nalozena
	//Desniklik na element, ki nas zanima - Preglej (inspect) - nas vrže na del strani, ki nas zanima
	//Izvedi prijavo
	var izvediPrijavo = function(){ 
		var uporabnik = document.querySelector("#uporabnisko_ime").value; //vzame vrednost polja uporabnisko ime, če ime ID - naslavljamo s #
		document.querySelector("#uporabnik").innerHTML = uporabnik; //innerhtml - teks, ki se nahaja znotraj elementa med span in /span
		document.querySelector(".pokrivalo").style.visibility = "hidden"; // - pri naslavljanju Classa dodamo . - .pokrivalo
	}
	
	document.querySelector("#prijavniGumb").addEventListener('click', izvediPrijavo); //document.querySelector - izbira nekega classa ali ID-ja 
	
	//Dodaj opomnik
	var dodajOpomnik = function(){
		var naziv_opomnika = document.querySelector("#naziv_opomnika").value; //Shranimo si tekst v spremenljivko
		var cas_opomnika = document.querySelector("#cas_opomnika").value;
		
		document.querySelector("#naziv_opomnika").value = ""; //Prazen tekst
		document.querySelector("#cas_opomnika").value = "";
		
		var opomniki = document.querySelector("#opomniki"); //Prepišemo kodo v navodilih, ki jo doda kot inner HTML v opomniki
		opomniki.innerHTML += " \
		<div class='opomnik'> \
		<div class='naziv_opomnika'>" + naziv_opomnika + "</div> \
		<div class='cas_opomnika'> Opomnik čez <span>" + cas_opomnika + "</span> sekund.\
		</div>";
	}
	document.querySelector("#dodajGumb").addEventListener('click', dodajOpomnik);
		
	//Posodobi opomnike
	var posodobiOpomnike = function() {
		var opomniki = document.querySelectorAll(".opomnik");
		
		for (var i = 0; i < opomniki.length; i++) {
			var opomnik = opomniki[i];
			var casovnik = opomnik.querySelector("span");
			var cas = parseInt(casovnik.innerHTML);
	
			//TODO: 
			// - če je čas enak 0, izpiši opozorilo "Opomnik!\n\nZadolžitev NAZIV_OPOMNIK je potekla!"
			// - sicer zmanjšaj čas za 1 in nastavi novo vrednost v časovniku
		}
	}
	setInterval(posodobiOpomnike, 1000);
	
});